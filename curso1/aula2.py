print("*********************************")
print("Bem vindo ao jogo de Adivinhação!")
print("*********************************", end="\n\n")

numero_secreto = 42

chute = int(input("Digite o seu número: "))
print("\nVocê digitou: ", chute, end="\n")
if (numero_secreto == chute ):
    print("Você acertou!")
else:
    print("Você errou!")

print("Fim do jogo")