import random

print("*********************************")
print("Bem vindo ao jogo de Adivinhação!")
print("*********************************", end="\n\n")

##random.seed(5) - se baseará neste valor para gerar um número
numero_secreto = random.randrange(1,101)
print(numero_secreto)
total_de_tentativas = 3
rodada = 1

for rodada in range(1, total_de_tentativas + 1):
    print("\nTentativa {} de {}".format(rodada, total_de_tentativas))
    chute = int(input("Digite um número entre 1 e 100: "))
    print("\nVocê digitou: ", chute, end="\n\n")

    if (chute < 1 or chute > 100):
        print("Você deve digitar um número entre 1 e 100!")
        continue  # continua para próxima rodada

    acertou = chute == numero_secreto
    maior = chute > numero_secreto
    menor = chute < numero_secreto

    if (acertou):
        print("Você acertou!\n\n")
        break
    else:
        if (maior):
            print("Você errou! O seu chute foi maior que o número secreto.", end="\n\n")
        elif (menor):
            print("Você errou! O seu chute foi menor que o número secreto.", end="\n\n")

print("Fim do jogo")
