print("*********************************")
print("Bem vindo ao jogo de Adivinhação!")
print("*********************************", end="\n\n")

numero_secreto = 42
total_de_tentativas = 3
rodada = 1

while(total_de_tentativas >= rodada):
    print("\nTentativa {} de {}".format(rodada,total_de_tentativas))
    chute = int(input("Digite o seu número: "))
    print("\nVocê digitou: ", chute, end="\n\n")

    acertou = chute == numero_secreto
    maior = chute > numero_secreto
    menor = chute < numero_secreto

    if (acertou):
        print("Você acertou!\n\n")
    else:
        if (maior):
            print("Você errou! O seu chute foi maior que o número secreto.",  end="\n\n")
        elif (menor):
            print("Você errou! O seu chute foi menor que o número secreto.",  end="\n\n")

    rodada = rodada+1

print("Fim do jogo")
