from jogos import forca, advinhacao


def escolhe_jogo():
    print("*********************************")
    print("*******Escolha o seu jogo!*******")
    print("*********************************")

    print("(1) Forca (2) Adivinhação")

    jogo = int(input("Qual jogo? "))

    if (jogo == 1):
        print("\nJogando forca\n")
        forca.jogar_forca()
    elif (jogo == 2):
        print("\nJogando adivinhação\n")
        advinhacao.jogar_advinhacao()

if(__name__ == "__main__"):
    escolhe_jogo()